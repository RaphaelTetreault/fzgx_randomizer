﻿using System.ComponentModel;

namespace LibGxFormat
{
	/// <summary>
	/// Defines the different games supported by this library.
	/// Some functions in this library require the game specification
	/// in order to handle game-specific differences.
	/// </summary>
	public enum AvGame
	{
		SuperMonkeyBall_1,
		SuperMonkeyBall_2,
		FZeroGX,
	}
}

