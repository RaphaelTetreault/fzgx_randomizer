﻿namespace StarkTools.IO
{
#if NET_4_7_3
    public enum EnumCompression
    {
        none,
        _8bit,
        _16bit,
        _32bit,
        _64bit,
    }
#endif
}