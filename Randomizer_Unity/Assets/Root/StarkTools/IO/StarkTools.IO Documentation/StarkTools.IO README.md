# StarkTools.IO Binary Serialization

API and Usage Documentation

## Overview

StarkTools.IO is a suite of structures and classes made for reading and writing tailored binary data.

- C# novice friendly, tailored for more experience devs, 

- Great for

- - importing/exporting/converting data from other programs
  - Managing lean save data
  - Managing custom file formats

## Topics

Below are other documents which explain various topics. They are laid out in sequence with later documents potentially referencing past material. However, more advanced users may wish to skip to the topic of choice.

* Introduction to Binary
* Reading and Writing Binary
  * Using pure C#
  * Using StarkTools.IO
* Reading and Writing SaveData